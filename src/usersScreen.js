import React, {useEffect, useState, userState} from "react";
import { View,Text,FlatList } from "react-native";
import axios from "axios";

const UserScreen = () => {

    const [users, setUsers] = useState([]);
    useEffect(() =>{
        getUsers()
    }, [])

    async function getUsers() {
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            setUsers(response.data);
        }
        catch(error){
            console.error(error);
        }
    }

        return(
            <View>
                <Text> Lista De Telefones </Text>
                <FlatList
                data={users}
                keyExtractor={(item)=> item.id.toString()}
                renderItem={({item }) => (
                    <View>
                        <Text>Telefone: {item.phone}</Text>
                    </View>
                )}
                
                
                
                />
            </View>
        )
};
export default UserScreen;
